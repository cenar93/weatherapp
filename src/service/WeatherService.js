import { WeatherApi } from "../api/WeatherApi";
import moment from "moment";

const filterByTimeranges = [
    ["00:00", "06:00"],
    ["06:00", "12:00"],
    ["12:00", "18:00"],
    ["18:00", "00:00"]
];
export const fetchWeatherData = async(lat, lon) => {
    const response = await WeatherApi.weather(lat, lon);
    const locationData = response.data.product.time;
    const weatherData = mapWeatherData(locationData);
    return mergeByDate(weatherData);
};

const mapWeatherData = locationData => {
    return locationData
        .filter(locDataPoint => locDataPoint.location.maxTemperature)
        .filter(locDataPoint => filterDaysFromToday(locDataPoint.from, 4))
        .map(locDataPoint => {
            const date = moment(locDataPoint.from).format("DD/MM/YY");
            return {
                date: date,
                data: {
                    from: moment(locDataPoint.from)
                        .utc()
                        .format("HH:mm"),
                    to: moment(locDataPoint.to)
                        .utc()
                        .format("HH:mm"),
                    maxTemperature: locDataPoint.location.maxTemperature.value,
                    minTemperature: locDataPoint.location.minTemperature.value,
                    symbol: locDataPoint.location.symbol,
                    precipitation: locDataPoint.location.precipitation
                }
            };
        })
        .filter(weatherDataPoint => filterByTimerange(weatherDataPoint));
};

const filterDaysFromToday = (date, nDaysFromToday) => {
    return moment(date).diff(moment(), "days") <= nDaysFromToday;
};

const mergeByDate = weatherData => {
    const mergedWeatherData = {};
    weatherData.forEach(weatherDataPoint => {
        if (!mergedWeatherData[weatherDataPoint.date]) {
            mergedWeatherData[weatherDataPoint.date] = [weatherDataPoint.data];
        } else {
            mergedWeatherData[weatherDataPoint.date].push(weatherDataPoint.data);
        }
    });
    return mergedWeatherData;
};

const filterByTimerange = weatherDataPoint => {
    return filterByTimeranges.some(
        timerange =>
        weatherDataPoint.data.from === timerange[0] ||
        weatherDataPoint.data.to === timerange[1]
    );
};