import { Request } from "./Request";

export const EnturAPI = {
    autocomplete: (text, size) =>
        Request.get(
            `https://api.entur.io/geocoder/v1/autocomplete?text=${text}&lang=no&size=${size}`, { "ET-Client-Name": "private-localUtvikling" }
        ).then(res => {
            return mapEnturData(res.data);
        })
};

const mapEnturData = data => {
    return data.features
        .filter(point => {
            return point.properties.category[0] === "GroupOfStopPlaces";
        })
        .map(point => {
            return {
                name: point.properties.name,
                coordinates: {
                    long: point.geometry.coordinates[0],
                    lat: point.geometry.coordinates[1]
                }
            };
        });
};