import { Request } from "./Request";

export const WeatherApi = {
        weather: (lat, lon) =>
            Request.get(
                `https://api.met.no/weatherapi/locationforecast/1.9/.json?${`lat=${lat}&lon=${lon}`}`
    ),
  symbol: id =>
    Request.get(
      `https://api.met.no/weatherapi/weathericon/1.1/?symbol=${id}&content_type=image/png`
    )
};