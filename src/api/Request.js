const axios = require('axios').default;

export const Request = {
    get: (url, headers) => axios.get(`${url}`, { headers: headers }),
    post: (url, data, headers) => axios.post(`${url}`)
}