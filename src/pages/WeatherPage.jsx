import React, { useEffect, useState } from "react";
import Loading from "../components/loading/Loading";
import WeatherTableList from "../components/tablelist/WeatherTableList";
import { fetchWeatherData } from "../service/WeatherService";
import { makeStyles } from "@material-ui/core";
import CitySelect from "../components/cityselect/CitySelect";

const useStyles = makeStyles({
  page: {
    display: "flex",
    justifyContent: "center",
    marginBottom: "2%",
    marginTop: "2%"
  },
  container: {
    display: "flex",
    flexDirection: "column"
  }
});

export default function WeatherPage() {
  const classes = useStyles();
  const [weatherData, setWeatherData] = useState();
  const [weatherDataLocation, setWeatherDataLocation] = useState({
    coordinates: { lat: 59.741164, long: 10.202492 },
    name: "Drammen"
  });

  useEffect(() => {
    getWeatherData(weatherDataLocation, setWeatherData);
  }, [weatherDataLocation]);

  if (!weatherData) {
    return <Loading />;
  }

  return (
    <div className={classes.page}>
      <div className={classes.container}>
        <CitySelect onCitySelected={setWeatherDataLocation} />
        <div>{weatherDataLocation.name}</div>
        <WeatherTableList weatherData={weatherData} />
      </div>
    </div>
  );
}

const getWeatherData = async (weatherDataLocation, setWeatherData) => {
    if (weatherDataLocation) {
      setWeatherData(null);
      const fetchedWeatherData = await fetchWeatherData(
        weatherDataLocation.coordinates.lat,
        weatherDataLocation.coordinates.long
      );
      setWeatherData(fetchedWeatherData);
    }
  };