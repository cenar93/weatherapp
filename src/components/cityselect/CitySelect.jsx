import React from "react";

import AsyncSelect from "react-select/async";
import { EnturAPI } from "../../api/EnturApi";

const loadOptions = (inputValue, callback) => {
  if (!inputValue) {
    return callback([]);
  }
  setTimeout(() => {
    EnturAPI.autocomplete(inputValue, 5).then(options =>
      callback(mapOptionsToValues(options))
    );
  }, 1000);
};

const mapOptionsToValues = options => {
  return options.map(option => ({
    value: option.coordinates,
    label: option.name
  }));
};

const mapSelectedCityData = cityData => {
    return {
      coordinates: cityData.value,
      name: cityData.label
    };
  };

const CitySelect = ({ onCitySelected }) => {
  const handleInputChange = newValue => {
    onCitySelected(mapSelectedCityData(newValue));
  };

  return (
    <div style={{zIndex: 100}}>
      <AsyncSelect
        cacheOptions
        loadOptions={loadOptions}
        defaultOptions
        onChange={handleInputChange}
      />
    </div>
  );
};
export default CitySelect;
