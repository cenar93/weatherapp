import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import uuid from "uuid";

const useStyles = makeStyles({
  table: {
    maxWidth: "500px"
  },
  container: {
    maxHeight: 1080,
    overflowX: "auto",
    display: "flex",
    justifyContent: "center"
  }
});

export default function WeatherTable({ weatherData }) {
  const classes = useStyles();
  return (
    <TableContainer component={Paper} className={classes.container}>
      <Table
        key={weatherData[0].from}
        stickyHeader
        className={classes.table}
        aria-label="sticky dense table"
      >
        <WeatherTableHead />
        <WeatherTableBody weatherData={weatherData} />
      </Table>
    </TableContainer>
  );
}

const WeatherTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <TableCell>Time</TableCell>
        <TableCell align="right">Temp</TableCell>
        <TableCell align="right">Forecast</TableCell>
        <TableCell align="right">Precipitation</TableCell>
      </TableRow>
    </TableHead>
  );
};

const WeatherTableBody = ({ weatherData }) => {
  return (
    <TableBody key={weatherData[0].from+"body"}>
      {weatherData.map(weather => (
        <WeatherTableRow key={uuid()} weather={weather} />
      ))}
    </TableBody>
  );
};

const WeatherTableRow = ({ weather }) => {
  return (
    <TableRow key={weather.from}>
      <TableCell component="th" scope="row">
        {weather.from + " - " + weather.to}
      </TableCell>
      <TableCell align="right">{weather.maxTemperature + "°"}</TableCell>
      <TableCell
        align="right"
        style={WeatherIconStyle(weather.symbol.number)}
      />
      <TableCell align="right">
        {weather.precipitation.value + " " + weather.precipitation.unit}
      </TableCell>
    </TableRow>
  );
};

const WeatherIconStyle = id => {
  return {
    background: `url(${`https://api.met.no/weatherapi/weathericon/1.1/?symbol=${id}&content_type=image/png)`} no-repeat`,
    backgroundPosition: "65% 25%"
  };
};
