import React from "react";
import DateCaption from "../caption/DateCaption";
import WeatherTable from "../table/WeatherTable";

const WeatherTableList = ({ weatherData }) => {
  return (
    <div>
      {Object.keys(weatherData).map(date => {
        return (
          <div key={date+"parent"}>
            <DateCaption date={date} />
            <WeatherTable weatherData={weatherData[date]} />
          </div>
        );
      })}
    </div>
  );
};

export default WeatherTableList
