import React from "react";
import moment from "moment-with-locales-es6";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  caption: {
    textAlign: "left"
  }
});

const DateCaption = ({ date }) => {
  const classes = useStyles()
  return (
    <div key={date} className={classes.caption}>
      <div key={date+"1"}>{convertDateToWeekday(date)+ "   "+date}</div>
    </div>
  );
};

const convertDateToWeekday = date => {
  const weekday = moment(date, "DD/MM/YY", "nb").format("dddd");
  return weekday.charAt(0).toUpperCase() + weekday.slice(1)
};

export default DateCaption;
